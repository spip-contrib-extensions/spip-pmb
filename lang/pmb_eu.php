<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pmb?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a' => ' -> ',
	'accedez_compte' => 'Zure kontu pertsonalean sartu',
	'accueil_site' => 'Ataria',
	'actualites' => 'Berriak',
	'adress_opac' => 'OPACaren helbidea',
	'ajouter_tag' => 'Etiquetez cet ouvrage',
	'annee_publication' => 'Argitaratze urtea',
	'auteur' => 'Egileak',
	'auteurs' => 'Egileak',
	'author_comment' => 'Oharra',
	'author_date' => 'Data',
	'author_lieu' => 'Helbidea',
	'author_pays' => 'Herrialdea',
	'author_ville' => 'Herria',
	'author_web' => 'Webgunea',
	'autres_lecteurs' => 'Dokumentu hau mailegatu duten bazkideek, hauek ere mailegatu dituzte',

	// B
	'bonjour' => 'Egun on',

	// C
	'catalogue' => 'Katalogoa',
	'changer_logo' => 'Zure argazkia',
	'code_barre' => 'Marra kodea',
	'code_barre_abbr' => 'MK',
	'collection' => 'Bilduma',
	'collection_issn' => 'ISSN',
	'collection_web' => 'Webgunea',
	'collections' => 'Bildumak',
	'collectivite' => 'Egitura',
	'collectivite_autre' => 'Beste egitura',
	'comment_ca_fonctionne' => 'Nola martxan da?',
	'compte_lecteur' => 'Zure irakurle kontua',
	'conferences' => 'P.U.L.P.',
	'config_pmb_logo' => 'PMB logoa',
	'config_pmb_logo_explication' => 'Afficher le logo de PMB dans la colonne ?',
	'config_pmb_partout' => 'Afficher la colonne de PMB partout sur le site ?',
	'config_pmb_partout_explication' => 'PMB peut surcharger les fichiers de Zpip pour
	afficher sa colonne quelque soit la page du site. Par défaut sa colonne n’est
	affichée que pour les pages concernant PMB. Mettre oui ici, l’affichera partout (ou presque).',
	'configurer_pmb' => 'SPIP-PMB -ren konfigurazioa',
	'configurer_rpc_json' => 'JSON-RPC (plus rapide, choisir par défaut)',
	'configurer_rpc_soap' => 'SOAP',
	'cote' => 'Kota',

	// D
	'date' => 'Data',
	'decouvrir' => 'ezagutu',
	'derniers_articles' => 'Berriak',
	'disponiblite' => 'Eskuragarritasuna',
	'disponiblite_abbr' => 'Esk.',
	'donner_avis' => 'Zure oharrak utzi',

	// E
	'ecrire_a' => 'Honi idatzi',
	'editeur' => 'Argitaletxea',
	'editeurs' => 'Argitaletxeak',
	'edition' => 'Mention d’édition',
	'en_savoir_plus' => 'gehiago irakurri +',
	'exemplaires' => 'Aleak',

	// F
	'format' => 'Formatua',

	// I
	'importance' => 'Garrantzia',
	'info_recherche_avancee' => 'Vous pouvez lancer une recherche portant sur un ou plusieurs mots<br />(titre, auteur, éditeur, ...)',
	'isbn' => 'ISBN/ISSN/EAN',

	// J
	'jsonrpc' => 'JSON-RPC zerbitzuaren helbidea',
	'jsonrpc_pw' => 'Pasahitza',

	// L
	'lire_la_suite' => 'segida irakurri',
	'localisation' => 'Nun',
	'localisation_abbr' => 'Nun',

	// M
	'message_compte_lecteur' => 'Prochainement, vous disposerez d’une int\\351gration compl\\350te\\nde votre compte lecteur sur ce site. \\n\\nEn attendant, vous allez \\352tre redirig\\351s vers l’interface standard.',
	'message_recherche_avancee' => 'Prochainement, vous disposerez d’une int\\351gration compl\\350te\\nde la recherche avanc\\351e sur ce site. \\n\\nEn attendant, vous allez \\352tre redirig\\351s vers l’interface standard.',
	'mon_compte' => 'Nere kontua',
	'mon_compte_necessite_identification' => 'Zure bazkide kontuan sartzeko identifikatu behar zara.',
	'mots_cles' => 'Gako hitzak',

	// N
	'newsletter' => 'Berripapera',
	'note_contenu' => 'Edukiari buruz',
	'note_generale' => 'Note générale',
	'notices_consultees' => 'kontsultatu notiziak',
	'nouveautes' => 'Katalogoaren berritasunak',
	'numero' => 'Zenbakia',

	// O
	'ouvrage_trouve' => 'titulu atxemanak',
	'ouvrages' => 'tituluak',
	'ouvrages_trouves' => 'titulu atxemanak',

	// P
	'parametrage_catalogue' => 'Katalogoaren parametratzea',
	'parametrage_options' => 'SPIP-PMB pluginaren aukerak',
	'parametrage_plugin' => 'pluginaren parametratzea',
	'pas_d_ouvrages_trouves' => '0 titulu atxemanak katalogoan',
	'pmb_login' => 'Identifikatzailea',
	'pmb_motdepasse' => 'Pasahitza',
	'presentation' => 'Aurkezpena',
	'prets_en_cours' => 'Maileguak',
	'prets_en_retard' => 'Maileguak berantean',
	'prix' => 'Prezioa',
	'publisher_address1' => 'Helbidea',
	'publisher_address2' => 'Helbidea (segida)',
	'publisher_city' => 'Herria',
	'publisher_country' => 'Herrialdea',
	'publisher_web' => 'Webgunea',
	'publisher_zipcode' => 'PK',

	// R
	'rang' => 'Rang',
	'recherche' => 'Bilaketa',
	'recherche_avancee' => 'Bilaketa aurreratua',
	'recherche_catalogue' => 'Katalogoan',
	'recherche_dans' => 'Hemen bilatu',
	'recherche_portail_web' => 'gunean',
	'rechercher_dans_catalogue' => 'Katalogoan bilatu',
	'reservation_ko' => 'Zure erreserba ezin izan dugu kontuan hartu.',
	'reservation_ok' => 'Zure erreserba kontuan hartu dugu.',
	'reservations' => 'Erreserbak',
	'reserver_ouvrage' => 'Titulu hau erreserbatu',
	'resultats' => 'Emaitzak',
	'resultats_dans_catalogue' => 'Emaitzak katalogoan',
	'resultats_dans_site' => 'Emaitzak gunean',
	'resultats_precedents' => 'Aitzineko emaitzak',
	'resultats_suivants' => 'Ondoko emaitzak',
	'retour' => 'Itzuli',
	'retour_recherche' => '[Itzuli bilaketaren emaitzetara]',
	'rpc_type' => 'Sareburu mota',
	'rubriques' => 'Nor gara?',

	// S
	'se_deconnecter' => 'Deskonektatu',
	'section' => 'Sekzioa',
	'serie' => 'Seriea',
	'session_expire' => 'Zure irakurle sesioaren epea gainditua da. Otoi deskonekta zaitez, eta berriro identifika zaitez.',
	'source' => 'Lien sur cette ressource',
	'sous_collection' => 'Azpi-bilduma',
	'support' => 'Euskarria',
	'sur' => '/',

	// T
	'titre' => 'Titulua',
	'titre_lien_pmb' => 'PMB, SIGB osoki librea',
	'tous_coupsdecoeur' => 'kritika guztiak',
	'toute_actualite' => 'berri guziak',
	'toutes_nouveautes' => 'berritasun guztiak',
	'type' => 'Mota',

	// U
	'unite_materielle' => 'Unité matérielle',
	'url' => 'url',

	// V
	'vous_etes_identifies' => 'Identifikatua zaude',

	// W
	'wsdl' => 'WSDL SOAP-ren helbidea'
);
